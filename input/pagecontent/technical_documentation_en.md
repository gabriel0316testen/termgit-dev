> **Für die deutsche Version bitte [hier](technical_documentation_de.md) klicken.**

### TerminoloGit

TerminoloGit allows to create, edit, and publish terminologies. This includes code systems, value sets as well as concept maps.

Our various **interfaces/API connection options** can be found under [Import of terminologies](use_cases_en.html#import-of-terminologies).

For a complete technical understanding of TerminoloGit, we recommend reading the documentation in the following order:
- [Architecture](architecture_en.md)
- [Set up your own TerminoloGit](setup_en.md)
- [File formats](file_formats_en.md)
- [UI features](ui_features_en.md)
- [How to use](use_cases_en.md)

In addition, an [analysis regarding some meta-information about this IG](ig_analysis.md) automatically created by the HL7® FHIR® IG publisher can be reviewed.

### Current state of development

The following two repositories serve to further develop TerminoloGit.

| Project Name | Repository URL | Description | GitLab Project ID | GitLab Pages URL |
| --- | --- | --- | --- | --- |
| TerminoloGit Dev | [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) | Repository for TerminoloGit technical development.<br/><br/>*Note:* The contents of the last successful pipeline of any Git branch of this repository are displayed under the specified GitLab Pages URL. | 21743825 | [https://elga-gmbh.gitlab.io/termgit-dev/](https://elga-gmbh.gitlab.io/termgit-dev/) |
| TerminoloGit Dev HTML | [https://gitlab.com/elga-gmbh/terminologit-dev-html](https://gitlab.com/elga-gmbh/terminologit-dev-html) | Repository for the static HTML pages created by the HL7® FHIR® IG Publisher based on the `master` branch of [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev). | 28239847 | [https://dev.termgit.elga.gv.at](https://dev.termgit.elga.gv.at) |

### Contributing

Thanks for your interest in contributing! In the spirit of the open source idea, the developers would be very happy about a reference to TerminoloGit, the use of this project or subprojects and even more about active participation. There are many ways to contribute to TerminoloGit. Get started with the [CONTRIBUTING.md in our Dev-Repo](https://gitlab.com/elga-gmbh/termgit-dev/-/blob/stable/CONTRIBUTING.md).

### Reference Implementations

#### Austrian e-Health Terminology Server

The first productive implementation of TerminoloGit was the Austrian e-Health terminology server which uses a variety of different terminologies with different structures from different international, national and local sources. This server was set into production in January 2022 and is actively used within the Austrian e-Health domain, especially the Austrian EHR (ELGA - Elektronische Gesundheitsakte). The current terminologies and governance can be found at [termgit.elga.gv.at](https://termgit.elga.gv.at).
