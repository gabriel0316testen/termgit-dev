> **For the english version please click [here](technical_documentation_en.md).**

### TerminoloGit

TerminoloGit ermöglicht es, Terminologien zu erstellen, zu bearbeiten und zu veröffentlichen. Dazu gehören Codesysteme, Value-Sets sowie Concept-Maps.

Unsere verschiedenen **Schnittstellen/API Anbindungs-Möglichkeiten** können unter [Importieren von Terminologien](use_cases_de.html#import-von-terminologien) nachgelesen werden.

Für ein vollständiges technisches Verständnis von TerminoloGit empfehlen wir die Dokumentation in folgnder Reihenfolge zu lesen:
- [Architektur](architecture_de.md)
- [eigenes TerminoloGit einrichten](setup_de.md)
- [Dateiformate](file_formats_de.md)
- [UI-Funktionen](ui_features_de.md)
- [Verwendung](use_cases_de.md)

Darüber hinaus kann eine [Analyse zu einigen Meta-Informationen über diesen IG](ig_analysis.md), die automatisch vom HL7® FHIR® IG Herausgeber erstellt wurde, eingesehen werden.

### Aktueller Stand der Entwicklung

Die folgenden beiden Repositories dienen der Weiterentwicklung von TerminoloGit.

| Projektname | Repository-URL | Beschreibung | GitLab-Projekt-ID | GitLab-Pages-URL |
| --- | --- | --- | --- | --- |
| TerminoloGit Dev | [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev) | Repository für die technische Entwicklung von TerminoloGit.<br/><br/>*Hinweis:* Der Inhalt der letzten erfolgreichen Pipeline eines beliebigen Git-Branches dieses Repositorys wird unter der angegebenen GitLab-Pages-URL angezeigt. | 21743825 | [https://elga-gmbh.gitlab.io/termgit-dev/](https://elga-gmbh.gitlab.io/termgit-dev/) |
| TerminoloGit Dev HTML | [https://gitlab.com/elga-gmbh/terminologit-dev-html](https://gitlab.com/elga-gmbh/terminologit-dev-html) | Repository für die statischen HTML-Seiten, die vom HL7® FHIR® IG Publisher erstellt werden, basierend auf dem `master`-Branch von [https://gitlab.com/elga-gmbh/termgit-dev](https://gitlab.com/elga-gmbh/termgit-dev). | 28239847 | [https://dev.termgit.elga.gv.at](https://dev.termgit.elga.gv.at) |

### Beitragen/Contributing

Vielen Dank für Ihr Interesse an einer Mitarbeit! Im Sinne des Open-Source-Gedankens würden sich die Entwickler sehr über einen Verweis auf TerminoloGit, die Nutzung dieses Projekts oder von Unterprojekten und vor allem über eine aktive Beteiligung freuen. Es gibt viele Möglichkeiten, zu TerminoloGit beizutragen. Beginnen Sie mit der [CONTRIBUTING.md in unserem Dev-Repo](https://gitlab.com/elga-gmbh/termgit-dev/-/blob/stable/CONTRIBUTING.md).

### Referenz-Implementierungen

#### Österreichischer e-Health Terminologie Server

Die erste produktive Implementierung von TerminoloGit war der österreichische e-Health Terminologieserver, der eine Vielzahl von verschiedenen Terminologien mit unterschiedlichen Strukturen aus verschiedenen internationalen, nationalen und lokalen Quellen verwendet. Dieser Server wurde im Januar 2022 in Betrieb genommen und wird in der österreichischen E-Health-Domäne, insbesondere in der österreichischen Elektronischen Gesundheitsakte (ELGA), aktiv genutzt. Die aktuellen Terminologien und die Governance können unter [termgit.elga.gv.at](https://termgit.elga.gv.at) eingesehen werden.
