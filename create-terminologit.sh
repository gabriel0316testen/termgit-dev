#!/bin/bash

# Write log to stdout
log () {
  echo " "
  echo "##############################"
  echo "$1"
  echo "##############################"
  echo " "
}

log_simple() {
  echo " "
  echo "------> $1"
  echo " "
}

log "START OF create-terminologit.sh"

# create directory where logs from specific commands will be preserved and will be made accessible via artifacts
# these logs have to be read together with the main pipeline log
log_simple "create logging directory"
rm -rvf log_appendix
mkdir -v log_appendix

# create temporary directory for old versions of resources (content will later be copied to IG's output directory)
log_simple "create directory for old versions"
mkdir -v old

# retrieve template for IG
log_simple "clone terminologit-template"
# opening up for templates that are not hosted on gitlab.com, like the original under "https://github.com/HL7/ig-template-base"
if ! [[ -z ${IG_PUB_TEMPLATE_URL+x} ]]
then
  if ! [[ -z ${IG_PUB_TEMPLATE_BRANCH+x} ]]
  then
    git clone -b ${IG_PUB_TEMPLATE_BRANCH} ${IG_PUB_TEMPLATE_URL} ./input/terminologit-template || exit 1
  else
    git clone ${IG_PUB_TEMPLATE_URL} ./input/terminologit-template || exit 1
  fi
else
  git clone https://gitlab.com/elga-gmbh/terminologit-template.git ./input/terminologit-template || exit 1
fi

# for integration tests (triggered by another pipeline) where no IGVer was executed
if [[ $CI_PIPELINE_SOURCE == "pipeline" ]]
then  
  log "Prepare includes for integration test"
  # create list of terminologies within "terminologies/CodeSystem*" and "terminologies/ValueSet*" directories
  find terminologies/CodeSystem* terminologies/ValueSet* -type f > resource_file_path_list.txt

  echo log_appendix/prepare_includes_integration_test.log
  python prepare_includes_integration_test.py > log_appendix/prepare_includes_integration_test.log || exit 1

  echo log_appendix/git_status_integration_test.log
  git status > log_appendix/git_status_integration_test.log
  log "Prepare includes for integration test finished"
fi

log "CREATE IG"

log_simple "copy arch_and_setup_*.md to their corresponding index.md and index_en.md"

# if ./input/pagecontent/index.md does not exist, copy ./input/pagecontent/technical_documentation_*.md to ./input/pagecontent/index*.md
[ ! -f "./input/pagecontent/index.md" ] && cp "./input/pagecontent/technical_documentation_de.md" "./input/pagecontent/index.md" && log_simple "copy ./input/pagecontent/technical_documentation_de.md to ./input/pagecontent/index.md"
[ ! -f "./input/pagecontent/index_en.md" ] && cp "./input/pagecontent/technical_documentation_en.md" "./input/pagecontent/index_en.md" && log_simple "copy ./input/pagecontent/technical_documentation_en.md to ./input/pagecontent/index_en.md"

# - if the param CI_DO_FULL_IGPUB_CREATION IS set,
#     do a full run of IG publisher by using the all_terminology_file_paths instead of terminology_file_paths
# for checking env variable (-z -> beware of negated value), this was used: https://stackoverflow.com/questions/3601515/how-to-check-if-a-variable-is-set-in-bash
if ! [[ -z ${CI_DO_FULL_IGPUB_CREATION_IN_BRANCH+x} ]]; then
  cp -rf all_terminology_file_paths.txt terminology_file_paths.txt
fi

# following python will alter the terminology_file_paths.txt file for individual logical groups per run
# if no terminology has been changed from the logical group, exit with a warning (exit 3)
log_simple "alter the terminology_file_paths.txt file for individual logical groups per run"
python divide_n_conquer.py > log_appendix/divide_n_conquer_output.log || exit $?

# copy .4.fhir.json files that had been changed in the initiating commit to ./input/resources
log_simple "copy resource files that had been changed in the initiating commit to ./input/resources"
# create temporary directory
mkdir -pv './input_resources'
echo log_appendix/copy_resources_to_input_resources.log
touch log_appendix/copy_resources_to_input_resources.log
# move all files in the list to terminologies (from https://superuser.com/questions/538306/move-a-list-of-filesin-a-text-file-to-a-directory)
for file in $(cat terminology_file_paths.txt); do
    cp -prv $file ./input_resources >> log_appendix/copy_resources_to_input_resources.log
    # dir=$(dirname "$file")
    # find $dir -name '*.4.fhir.json' -exec cp -prv '{}' './input_resources' ';' >> log_appendix/copy_resources_to_input_resources.log
done
# remove everything from input/resources
echo log_appendix/delete_cs_and_vs_for_no_terminologies.log
touch log_appendix/delete_cs_and_vs_for_no_terminologies.log
find ./input/resources/ -name 'CodeSystem-*' -exec rm -rvf '{}' ';' >> log_appendix/delete_cs_and_vs_for_no_terminologies.log
find ./input/resources/ -name 'ValueSet-*' -exec rm -rvf '{}' ';' >> log_appendix/delete_cs_and_vs_for_no_terminologies.log
find ./input/resources/ -name 'ConceptMap-*' -exec rm -rvf '{}' ';' >> log_appendix/delete_cs_and_vs_for_no_terminologies.log
# move all terminologies relevant for this run to input/resources
mv -v ./input_resources/* ./input/resources/

# Remove '.4.fhir' from file names in order to make them detectable by IG publisher
log_simple "remove '.4.fhir' from file names"
echo log_appendix/rename_4.fhir.json_to_json.log
find ./input/resources -type f -name '*.4.fhir.json' | while read FILE ; do
    newfile="$(echo ${FILE} |sed -e 's/.4.fhir//')" ;
    mv -v "${FILE}" "${newfile}" >> log_appendix/rename_4.fhir.json_to_json.log ;
done

# Log which files the directory input/resources contains
log_simple "Files within input/resources (to be processed by IG publisher)"
ls ./input/resources

# set canoncial and FHIR version for Implementation Guide resource
# if there is a local variable set for this matrix job, than use that
if ! [[ -z ${TERMGIT_FHIRVERSION_LOCAL+x} ]]; then
  TERMGIT_FHIRVERSION=$TERMGIT_FHIRVERSION_LOCAL
fi
log_simple "set canoncial to $TERMGIT_CANONICAL and fhir version to $TERMGIT_FHIRVERSION"
sed -i "s|TERMGIT_CANONICAL|$TERMGIT_CANONICAL| ; s|TERMGIT_FHIRVERSION|$TERMGIT_FHIRVERSION|" sushi-config.yaml
log_simple "canonical has been set"

log_simple "prepare pagecontent for IG publisher"
# replace all ".md" with ".html" in order to have working links in IG
sed -i "s|\.md|\.html|gI" input/pagecontent/*
# replace all "(input/files" with "(files" to have working file references
sed -i "s|(input/files|(files|gI" input/pagecontent/*
# replace all "(input/images" with "(images" to have working image references
sed -i "s|(input/images/|(|gI" input/pagecontent/*
log_simple "pagecontent successfully prepared"

# run sushi for creating ImplementationGuid resource
log_simple "run sushi"
sushi || exit 1
log_simple "sushi finished"

# if IG_PUBLISHER_URL is set use the value of the environment variable and download the IG publisher from there
if ! [[ -z "$IG_PUBLISHER_URL" ]];
then
  # download IG publisher
  log_simple "download IG publisher"
  curl -L ${IG_PUBLISHER_URL} -o "$PWD/input-cache/publisher.jar" --create-dirs
  log_simple "IG publisher downloaded"
else
  # copy the IG publisher provided in the docker container
  log_simple "copy IG publisher"
  # make sure ./input-cache/ exists
  mkdir -pv ./input-cache/
  # copy IG publisher
  cp -v /opt/publisher.jar ./input-cache/publisher.jar
  log_simple "IG publisher copied"
fi

# run IG publisher:
log_simple "run IG publisher"
unset DISPLAY
# IG publisher will be executed. If executtion of IG publisher fails, this script will fail and, thus, the pipeline as well.
./_genonce.sh || exit 1
log_simple "IG publisher finished"

# increase stack size
# https://stackoverflow.com/questions/10035541/what-causes-a-python-segmentation-fault
# relevant especially for: ConceptMap-snomed-orphanet-mapping.xml (reason could not be determined)
ulimit -s 32768

log_simple "activate previous version links"
# remove all "<!--COMMENTED-PREVIOUS-VERSION-INCLUDE" and "COMMENTED-PREVIOUS-VERSION-INCLUDE-->" in order to activate the previous version links
sed -i "s|<!--COMMENTED-PREVIOUS-VERSION-INCLUDE||gI" output/*.previous-versions.html
sed -i "s|COMMENTED-PREVIOUS-VERSION-INCLUDE-->||gI" output/*.previous-versions.html
log_simple "previous version links activated"

# usually minify all HTML files created by IG publisher
# if MINIFY_RESULTING_HTMLS is set for this matrix job, than do minify all the resulting htmls
if ! [[ -z ${MINIFY_RESULTING_HTMLS+x} ]]; then
  log_simple "minify HTML files"
  echo log_appendix/minify_html_output.log
  python minify_html_output.py > log_appendix/minify_html_output.log || exit 1
fi

# copy content from "./old" (contains redirects and all old versions of resources) to "./output"
log_simple "copy old versions to output directory"
echo log_appendix/copy_old_to_output.log
cp -vR ./old/* ./output/ > log_appendix/copy_old_to_output.log

# create sitemap
log_simple "create and copy information about sitemap to output directory"
python sitemap_generator.py || exit 1
cp -v ./input/sitemap/* ./output/

# Copy new redirects files in order to be committed properly
log_simple "copy new redirects files in order to be committed"
cat copy_created_redirects.txt
mkdir -pv ./input/redirects/CodeSystem
mkdir -pv ./input/redirects/ValueSet
while read -r line
do
  eval $line
done < copy_created_redirects.txt

log "IG CREATED"

# for the multiple divide and conquer runs with their logical groups
# create dirs (mv cant do that)
mkdir -pv ./output_per_logical_group/$TERMINOLOGY_LOGICAL_GROUP/ ./removed_from_output/$TERMINOLOGY_LOGICAL_GROUP/ ./log_appendix/$TERMINOLOGY_LOGICAL_GROUP/
# move files that should not be published to a separate directory for them to be available if needed
mv -v ./output/*.db ./output/*.zip ./output/*.tgz ./output/*.xlsx ./output/*.pack ./output/expansions.* ./output/qa.min.html ./output/qa.xml ./output/qa.txt ./output/qa.html ./output/qa-tx.html ./output/qa-eslintcompact.txt ./output/qa-time-report.json ./removed_from_output/$TERMINOLOGY_LOGICAL_GROUP/
mv -v ./output/* ./output_per_logical_group/$TERMINOLOGY_LOGICAL_GROUP/
mv -v ./log_appendix/* ./terminology_file_paths.txt ./terminology_file_paths_without_grouping.txt ./log_appendix/$TERMINOLOGY_LOGICAL_GROUP/

log "END OF create-terminologit.sh"
