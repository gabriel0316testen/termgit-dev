import csv
print('the valuesets.md, codesystems.md and conceptmaps.md are now beeing created in input/pagecontent out of the terminologies/terminologiesMetadata.csv')
# for information how the columns there look like:
#['name', 'canonical', 'oid', 'version', 'id', 'type', 'metadata-change-timestamp', 'description', 'title', 'status']
metadata_iterator = csv.DictReader(file_reading_metadata := open("./terminologies/terminologiesMetadata.csv"))
resource_types_for_own_artifacts_page = ["CodeSystem", "ValueSet", "ConceptMap"]
header = "\n| Title & Current Version | URL & OID | Description |\n| --- | --- | --- |\n"
for one_resource_type in resource_types_for_own_artifacts_page:
   with open("./input/pagecontent/"+one_resource_type.lower()+"s.md", "w") as file:
      file.write(header)
      are_there_any_retired_ones = False
      for row in metadata_iterator:
         if row.get('type') == one_resource_type:
            if row.get('status') != "retired":
               file.write("|["+(row.get('title') or row.get('id'))+"]("+row.get('name')+".html)<br/>"+(row.get('version') or "")+"|"+
                     (row.get('canonical') or "")+"<br/>"+(row.get('oid') or "")+"|"+(row.get('description') or "").strip('"')+"|\n")
            else: are_there_any_retired_ones = True
      file_reading_metadata.seek(0)
      if are_there_any_retired_ones:
         file.write("\n\n### Retired "+one_resource_type+"s:\n")
         file.write(header)
         for row in metadata_iterator:
            if row.get('type') == one_resource_type and row.get('status') == "retired":
               file.write("|["+(row.get('title') or row.get('id'))+"]("+row.get('name')+".html)<br/>"+(row.get('version') or "")+"|"+
                     (row.get('canonical') or "")+"<br/>"+(row.get('oid') or "")+"|"+(row.get('description') or "").strip('"')+"|\n")
         file_reading_metadata.seek(0)